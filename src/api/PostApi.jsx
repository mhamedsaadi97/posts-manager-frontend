import axios from 'axios';

const BASE_URL = 'http://127.0.0.1:8000/api/v1';

export const fetchPosts = async (page) => {
    try {
      const response = await axios.get(`${BASE_URL}/posts?page=${page}`);
     
      return response.data;
      
    } catch (error) {
      throw new Error('Error fetching posts: ' + error.message);
    }
  };
  
  export const searchPosts = async (query) => {
    try {
      const response = await axios.get(`${BASE_URL}/posts/search?q=${query}`);
      
      return response.data;
    } catch (error) {
      throw new Error('Error searching posts: ' + error.message);
    }
  };

  export const fetchPost = async (postId) => {
    try {
      const response = await axios.get(`${BASE_URL}/posts/${postId}`);
      
      return response.data;
    } catch (error) {
      throw new Error('Error searching posts: ' + error.message);
    }
  };


  export const createPost = async (postData) => {
    try {
      const response = await axios.post(`${BASE_URL}/posts`, postData,{ headers: {
        'Content-Type': 'multipart/form-data'
    }});
      
      return response.data;
    } catch (error) {
      throw new Error('Error creating post: ' + error.message);
    }
  };

  export const editPost = async (postId,postData) => {
    console.log(postData);
    try {
      const response = await axios.put(`${BASE_URL}/posts/${postId}`, postData);
      
      return response.data;
    } catch (error) {
      throw new Error('Error creating post: ' + error.message);
    }
  };


  export const deletePost = async (postId,onSuccess, onError) => {
    try {
      const response = await axios.delete(`${BASE_URL}/posts/${postId}`);
      onSuccess();
    } catch (error) {
      onError(error);
    }
  };