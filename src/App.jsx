import React from 'react';
import PostsTable from './components/PostsTable';
import SearchBar from './components/SearchBar';
import CreatePostButton from './components/CreatePostButton';
import usePosts from './hooks/usePosts';
import './output.css';



function App() {

  const { posts, loading, error, currentPage, totalPages, handleSearch, handlePageChange,handleCancelSearch} = usePosts();

   
  return (
    
    <div className="container mx-auto mt-8">
      <h1 className="text-lg font-semibold mb-4 uppercase ...">Posts Manager</h1>

      <div className="flex">
      <div className="w-3/4">
      <SearchBar onSearch={handleSearch} onClose={handleCancelSearch} />
      </div>
      <div className="w-1/4 text-right">
      <CreatePostButton />
      </div>
    </div>
      
      <PostsTable
        posts={posts}
        loading={loading}
        error={error}
        onPageChange={handlePageChange}
        onDelete={handleCancelSearch}
        currentPage={currentPage}
        totalPages={totalPages}
      />
    </div>
  )
}

export default App
