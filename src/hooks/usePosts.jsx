import { useState, useEffect } from 'react';
import { fetchPosts, searchPosts } from '../api/PostApi';

const usePosts = () => {
  const [posts, setPosts] = useState([]);
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(null);
  const [currentPage, setCurrentPage] = useState(1);
  const [totalPages, setTotalPages] = useState(1);

  useEffect(() => {
    const fetchData = async () => {
      setLoading(true);
      try {
        const response = await fetchPosts(currentPage);
        setPosts(response.data);
        setTotalPages(response.meta.last_page);
      } catch (error) {
        setError(error.message);
      }
      setLoading(false);
    };

    fetchData();
  }, [currentPage]);

  const handleSearch = async (searchRes) => {
    setLoading(true);
    try {
      
      setPosts(searchRes.data);
      setTotalPages(searchRes.meta.last_page);
    } catch (error) {
      setError(error.message);
    }
    setLoading(false);
  };

  const handlePageChange = (page) => {
    setCurrentPage(page);
  };

  const handleCancelSearch = async() => {
    setLoading(true);
      try {
        const response = await fetchPosts(currentPage);
        setPosts(response.data);
        setTotalPages(response.meta.last_page);
      } catch (error) {
        setError(error.message);
      }
      setLoading(false);
  };

  return { posts, loading, error, currentPage, totalPages, handleSearch, handlePageChange,handleCancelSearch};
};

export default usePosts;