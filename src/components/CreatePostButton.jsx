import React from 'react';
import {useNavigate} from 'react-router-dom';

const CreatePostButton = () => {

  const navigate = useNavigate();
  const handleCreatePost = () => {
    navigate('/create-post');

  }

  return (
    <button onClick={ () =>handleCreatePost()} className="px-4 py-2 bg-green-500 text-white rounded-md hover:bg-green-600 focus:outline-none">
      Create Post
    </button>
  );
};

export default CreatePostButton;