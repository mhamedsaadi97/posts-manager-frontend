import React from 'react';
import { css } from '@emotion/react';
import { ClipLoader } from 'react-spinners';

const Spinner = ({ loading }) => {
  const override = css`
    display: block;
    margin: 0 auto;
    border-color: red;
  `;

  return (
    <div className="flex justify-center items-center mt-4">
      <ClipLoader color={'#000'} loading={loading} css={override} size={35} />
    </div>
  );
};

export default Spinner;