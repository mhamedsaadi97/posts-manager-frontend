import React, { useState,useEffect} from 'react';
import { createPost } from '../api/PostApi';
import {useNavigate,useParams} from 'react-router-dom';
import { fetchPost ,editPost } from '../api/PostApi';
import toastr from 'toastr';
import 'toastr/build/toastr.min.css';


const EditPostForm = () => {
    const [name, setName] = useState('');
    const [description, setDescription] = useState('');
    const [category, setCategory] = useState('');
    const [thumbnail, setThumbnail] = useState(null);
    const [loading, setLoading] = useState(false);
    const [error, setError] = useState(null);
    const [descriptionError, setDescriptionError] = useState('');
    const [categoryError, setCategoryError] = useState('');
    const [thumbnailError, setThumbnailError] = useState('');
    const [nameError, setNameError] = useState('');
    const navigate = useNavigate();
    const { id } = useParams();

    useEffect(() => {
        const fetchPostData = async() => {
    
            try {
              const response = await fetchPost(id);
              setName(response.data.name);
              setDescription(response.data.description);
              setCategory(response.data.category);
              setThumbnail(response.data.thumbnail);
            } catch (error) {
              setError(error.message);
            }
            
        };
        fetchPostData();
      }, [id]);

    

    const handleBack = () => {
        navigate('/');
      };
      const handleChange = (e) => {
        setThumbnail(e.target.files[0]);
    };
  
    const handleSubmit = async (e) => {
      e.preventDefault();

      if (!name.trim()) {
        setNameError('Title is required');
        return;
      } else {
        setNameError('');
      }
      if (!description.trim()) {
        setDescriptionError('Description is required');
        return;
      } else {
        setDescriptionError('');
      }

      if (!category.trim()) {
        setCategoryError('Category is required');
        return;
      } else {
        setCategoryError('');
      }

      setLoading(true);
      try {
        await editPost(id,{ name : name, description:description,category:category,thumbnail:thumbnail });
        console.log('Post edited successfully');
        setName('');
        setDescription('');
        setCategory('');
        setThumbnail('');
        toastr.success('Post Updated successfully!', 'Success');
        navigate('/');
      } catch (error) {
        setError(error.message);
        toastr.error('Error Upadating post. Please try again.', 'Error');
      }
      
    };
  
    return (




        <div className="flex justify-center items-center h-screen bg-gray-100">
         <div className="bg-white rounded-lg shadow-lg p-8 w-3/5">
          <h2 className="mt-16 text-2xl font-bold mb-4 text-center">Edit Post</h2>
          {error && <div className="text-red-500 mb-4 text-center">{error}</div>}
          <form onSubmit={handleSubmit} className="space-y-4" encType="multipart/form-data">
          <div>
            <label htmlFor="title" className="block font-medium text-lg mb-1">Title</label>
            <input
              type="text"
              id="title"
              value={name}
              onChange={(e) => setName(e.target.value)}
              className="mt-1 px-4 py-2 w-full border rounded-md shadow-md focus:outline-none focus:border-blue-500"
              
            />
            {nameError && <div className="text-red-500 mt-1 text-sm">{nameError}</div>}
          </div>
          <div>
            <label htmlFor="content" className="block font-medium text-lg mb-1">Description</label>
            <textarea
              id="content"
              value={description}
              onChange={(e) => setDescription(e.target.value)}
              className="mt-1 px-4 py-2 w-full border rounded-md shadow-md focus:outline-none focus:border-blue-500"
              rows="4"
              
            ></textarea>
            {descriptionError && <div className="text-red-500 mt-1 text-sm">{descriptionError}</div>}
          </div>
          <div>
            <label htmlFor="category" className="block font-medium text-lg mb-1">Category</label>
            <input
              type="text"
              id="category"
              value={category}
              onChange={(e) => setCategory(e.target.value)}
              className="mt-1 px-4 py-2 w-full border rounded-md shadow-md focus:outline-none focus:border-blue-500"
              
            />
            {categoryError && <div className="text-red-500 mt-1 text-sm">{categoryError}</div>}
          </div>
          <div>
            <label htmlFor="thumbnail" className="block font-medium text-lg mb-1">Thumbnail</label>
            
            <input className="mt-1 px-4 py-2 w-full border rounded-md shadow-md focus:outline-none focus:border-blue-500"
             type="file" onChange={handleChange} />
             {thumbnail && <img className='w-20' src={`http://127.0.0.1:8000/storage/${thumbnail}`} alt={name} />}
            {thumbnailError && <div className="text-red-500 mt-1 text-sm">{thumbnailError}</div>}
          </div>
            <div className="text-center">
            <button type="submit" className="flex-1 px-6 py-3 bg-blue-500 text-white rounded-md hover:bg-blue-600 focus:outline-none">
              {loading ? 'Editing...' : 'Edit Post'}
            </button>

            <button type="button" onClick={handleBack} className="px-4 py-2 outline outline-2 outline-offset-2 rounded-md  focus:outline-none absolute top-0 right-0 m-4">
              Back to Posts
            </button>
            </div>
          </form>
        </div>
      </div>




    );
  };
  
  export default EditPostForm;