import React, { useState } from 'react';
import { searchPosts } from '../api/PostApi';

const SearchBar = ({ onSearch, onClose }) => {
  const [searchTerm, setSearchTerm] = useState('');

  const handleSearchChange = (event) => {
    setSearchTerm(event.target.value);
  };

  const handleSearch = async () => {
    try {
      const response = await searchPosts(searchTerm);
      onSearch(response); 
    } catch (error) {
      console.error('Error searching posts:', error.message);
    }
  };

  const handleClear = async () => {
    setSearchTerm('');
    onClose(); 
  };

  return (
    <div className="mb-6">
      <input
        type="text"
        placeholder="Search..."
        className="px-4 py-2 border rounded-md focus:outline-none"
        value={searchTerm}
        onChange={handleSearchChange}
      />
      <button onClick={handleSearch} className="px-4 py-2 bg-blue-500 text-white rounded-md hover:bg-blue-600 focus:outline-none">
        Search
      </button>
      {searchTerm && (
        <button onClick={handleClear} className="ml-2 px-4 py-2   rounded-md  focus:outline-none">
          Clear Search
        </button>
      )}
    </div>
  );
};

export default SearchBar;