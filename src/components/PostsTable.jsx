import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faEdit, faTrash } from '@fortawesome/free-solid-svg-icons';
import { deletePost} from '../api/PostApi';
import Spinner from './Spiner';
import {useNavigate} from 'react-router-dom';

const PostsTable = ({ posts, loading, error, onPageChange, currentPage, totalPages,onDelete}) => {


  const handlePageChange = (page) => {
    onPageChange(page);
  };
  const navigate = useNavigate();
  const handleEdit = (postId) => {
    navigate(`/edit-post/${postId}`); 
  };

  const handleDelete = async (postId) => {
    const onSuccess = () => {
      onDelete();
    };

    const onError = (error) => {
      console.error('Error deleting post:', error);
    };

    await deletePost(postId, onSuccess, onError);
  };


  return (


    <div className="">
      {loading && <div className="text-center"><Spinner loading={loading} /></div>}
      {error && <div className="text-center text-red-500">Error: {error}</div>}
      <table className="w-full bg-white rounded-md shadow">
        <thead>
          <tr className="bg-gray-100">
          <th className="px-4 py-2">ID</th>
          <th className="px-4 py-2">Thumbnail</th>
            <th className="px-4 py-2">Title</th>
            <th className="px-4 py-2">Category</th>
            <th className="px-4 py-2">Description</th>
            <th className="px-4 py-2">Date</th>
            <th className="px-4 py-2">Actions</th>
          </tr>
        </thead>
        <tbody>
          {posts.map((post) => (
            <tr key={post.id} className="hover:bg-gray-50">
              <td className="px-4 py-2 text-center">{post.id}</td>
              <td className="px-4 py-2 text-center">
                
                <img className='w-10' src={`http://127.0.0.1:8000/storage/${post.thumbnail}`}/>
                </td>
              <td className="px-4 py-2 text-center">{post.name}</td>
              <td className="px-4 py-2 text-center">{post.category}</td>
              <td className="px-4 py-2 text-center">{post.description}</td>
              <td className="px-4 py-2 text-center">{new Date(post.created_at).toLocaleDateString('en-US')}</td>
              <td className="px-4 py-2 text-center">
                
                <button onClick={() => handleEdit(post.id)} className="mr-2 ml-2">
                  <FontAwesomeIcon icon={faEdit} />
                </button>
                <button onClick={() => handleDelete(post.id)} className="mr-2 ml-2">
                  <FontAwesomeIcon icon={faTrash} />
                </button>
              </td>
            </tr>
          ))}
        </tbody>
      </table>
      <div className="mt-6 flex justify-center">
        {/* Pagination */}
        {Array.from({ length: totalPages }, (_, i) => (
          <button
            key={i}
            onClick={() => handlePageChange(i + 1)}
            className={`mx-1 px-3 py-1 rounded-md ${
              currentPage === i + 1 ? 'bg-blue-500 text-white' : 'bg-gray-200'
            }`}
          >
            {i + 1}
          </button>
        ))}
      </div>
    </div>

  );
};

export default PostsTable;
